<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Quiz;
use App\Question;

class QuestionController extends Controller
{
    public function add(Quiz $quiz)
    {
      return view('questions.add', compact('quiz'));
    }
    
    public function store(Request $request, Quiz $quiz)
    { 
      $convertYoutubeEnd = ($request -> youtubeEnd == "") ? null : $request -> youtubeEnd;
      
      $this->validate( $request, [
        'question' => 'required',
        'answer1' => 'required',
        'answer2' => 'required',
        'answer3' => 'required',
        'correctAnswer' => 'required',
        'ordinal' => 'required',
      ]);
      
      $question = new Question();
      $question->question = $request-> question;
      $question->answer1 = $request-> answer1;
      $question->answer2 = $request-> answer2;
      $question->answer3 = $request-> answer3;
      $question->correct = $request-> correctAnswer;
      $question->quiz_id = $quiz-> id;
      $question->ordinal = $request-> ordinal;
      $question->youtube_id = $request-> youtubeId;
      $question->youtube_start = $request -> youtubeStart;
      $question->youtube_end = $convertYoutubeEnd;
      $question->save();
      
      return back();
    }
    
    public function edit(Question $question)
    {
      $answers = [$question->answer1, $question->answer2, $question->answer3];
      $youtubeStartNum = $question->youtube_start;
      $youtubeEndNum = $question->youtube_end;
      $ordinal = $question->ordinal;
      
      return view('questions.edit', ['answers' => $answers, 
                                     'question' => $question,
                                     'youtubeStartNum' => $youtubeStartNum,
                                     'youtubeEndNum' => $youtubeEndNum,
                                     'ordinal' => $ordinal,
                                    ]);
    }
    
    public function update(Request $request, Question $question)
    {
        $convertYoutubeEnd = ($request -> youtubeEnd == "") ? null : $request -> youtubeEnd;
        
        $this->validate( $request, [
          'question' => 'required',
          'answer1' => 'required',
          'answer2' => 'required',
          'answer3' => 'required',
          'correctAnswer' => 'required',
          'ordinal' => 'required',
        ]);
        
        $question = Question::find($question->id);
        $question->question = $request-> question;
        $question->answer1 = $request-> answer1;
        $question->answer2 = $request-> answer2;
        $question->answer3 = $request-> answer3;
        $question->correct = $request-> correctAnswer;
        $question->ordinal = $request-> ordinal;
        $question->youtube_id = $request-> youtubeId;
        $question->youtube_start = $request-> youtubeStart;
        $question->youtube_end = $convertYoutubeEnd;
        $question->save();
        
        return back();
    }
    
    public function destroy(Quiz $quiz, Question $question)
    {         
        $question->delete();
        return back();
    }
}
