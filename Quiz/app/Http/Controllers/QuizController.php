<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Quiz;

class QuizController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {   
      $quizzes = Quiz::all();
    
      return view('home', compact('quizzes'));
  }
  
  public function show(Quiz $quiz)
  {   
      return view('quizzes.show', compact('quiz'));
  }
  
  public function add()
  {   
      return view('quizzes.add');
  }
  
  public function store(Request $request)
  {   
      $this->validate( $request, [
        'quiz_title' => 'required'
      ]);
      
      $quiz = new Quiz();
      $quiz->title = $request -> quiz_title;
      $quiz->save();
      
      return back();
  }
  
  public function edit(Quiz $quiz)
  {   
      return view('quizzes.edit',compact('quiz'));
  }
  
  public function update(Request $request, Quiz $quiz)
  {         
      $this->validate( $request, [
        'quiz_title' => 'required'
      ]);
      
      $quiz = Quiz::find($quiz->id);
      $quiz->title = $request-> quiz_title;
      $quiz->save();
      
      return back();
  }
  
  public function destroy(Quiz $quiz)
  {         
      $quiz->delete();
      return back();
  }
  
}
