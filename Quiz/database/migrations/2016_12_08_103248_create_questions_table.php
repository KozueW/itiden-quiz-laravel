<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
          $table->increments('id');
          $table->char('question',255);
          $table->char('answer1',255);
          $table->char('answer2',255);
          $table->char('answer3',255);
          $table->tinyInteger('correct');
          $table->integer('quiz_id');
          $table->integer('ordinal');
          $table->char('youtube_id',25);
          $table->integer('youtube_start')->default('0');
          $table->integer('youtube_end')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
