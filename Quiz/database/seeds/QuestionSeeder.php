<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //delete all data.
      Question::truncate();
      
      $questions = array(
        array(1, 'World of Warcraft släpptes i Europa den?', '5 feb 2005', '11 feb 2005', '25 feb 2005', 2, 1, 1, '', 0, NULL),
        array(2, 'Böckerna om Sherlock Holmes skrevs av?', 'Edgar Allan Poe', 'Richard Doyle', 'Arthur Conan Doyle', 3, 1, 2, '', 0, NULL),
        array(3, 'Namnet på Morpheus skepp i The Matrix?', 'Nebrachanezzar', 'Nebuchadnezzar', 'Neochanezzar', 2, 1, 3, '', 0, NULL),
        array(4, 'Vad heter VDn för Adobe?', 'Shantanu Narayen', 'John Warnock', 'Charles Geschke', 1, 1, 4, '', 0, NULL),
        array(5, 'Kallas ett golfhål som viker av åt höger eller vänster efter det tänkta utslagets placering på fairway?', 'Hook', 'Dogleg', 'Slice', 2, 1, 5, '', 0, NULL),
        array(6, 'I vilken division ligger Hönö IS a-lag? ', '6c Göteborg ', '5a Göteborg', '5b Göteborg', 3, 1, 6, '', 0, NULL),
        array(7, 'vilken radie från hålet räknas det som putt i disc golf?', '5m', '8m', '10m', 3, 1, 7, '', 0, NULL),
        array(8, 'I vilket land ligger det berg som sträcker sig högst från centrum av jorden? ', 'Ecuador ', 'Argentina ', 'Nepal ', 1, 1, 8, '', 0, NULL),
        array(9, 'Vilken av följande drycker distruberas inte av coca-cola company? ', 'Fanta', 'Sprite', '7up', 3, 1, 9, '', 0, NULL),
        array(10, 'Från vilket land kommer ölen Kronenbourg? ', 'Frankrike', 'Tyskland', 'Holland ', 1, 1, 10, '', 0, NULL),
      );

      //insert
      for($i=0;$i < count($questions);$i++)
      {
        Question::create(array(
                  'id' => $questions[$i][0],
                  'question' => $questions[$i][1],
                  'answer1' => $questions[$i][2],
                  'answer2' => $questions[$i][3],
                  'answer3' => $questions[$i][4],
                  'correct' => $questions[$i][5],
                  'quiz_id' => $questions[$i][6],
                  'ordinal' => $questions[$i][7],
                  'youtube_id' => $questions[$i][8],
                  'youtube_start' => $questions[$i][9],
                  'youtube_end' => $questions[$i][10],
        ));

      }
    }
}
