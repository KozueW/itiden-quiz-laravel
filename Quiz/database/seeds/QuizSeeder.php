<?php

use Illuminate\Database\Seeder;
use App\Quiz;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //delete all data.
      Quiz::truncate();
      
      $quizzes = ['Itiden sommar 2011','Itiden julbord 2011','Itiden julbord 2012',
                  'Sommaravslutning 2013','After Work mars 2013','Quizenbreak Juli 2013',
                  'Quizmaz 2013','QuizTime 2013','Julbord 2014'];

      //insert
      for($i=0;$i < count($quizzes);$i++)
      {
        Quiz::create(array(
                  'id' => $i+1,
                  'title' => $quizzes[$i]
        ));

      }
      
    }
}
