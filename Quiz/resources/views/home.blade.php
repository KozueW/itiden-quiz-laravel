@extends('layouts.app')

@section('content')
      
    <div class='row'>
      <div class='col-md-6 col-md-offset-3'>
        <h1>All quizzes</h1>
        
        <ul class='list-group'>
          
          @foreach ($quizzes as $quiz)
            <li class='list-group-item' style="opacity:1.0;">
  
              {{ $quiz -> title }}

              <div style="float:right">
                <a style="display: inline-block;"　href="/quizzes/{{ $quiz -> id }}/edit">
                  edit
                </a>
                /
                <form style="display: inline-block;" method="POST" action="/quizzes/{{ $quiz->id }}/delete" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <div class="form-group">
                   <button style="padding:0px; padding-bottom:2px;" type="submit" class="btn btn-link">delete</button>
                  </div>
                </form>
              </div>
            </li>
          @endforeach
          
        </ul>
        
        <button onclick="window.location='{{ url("quizzes/add") }}'"  class='btn btn-default'>Add quiz</button>
       
      </div>
    </div>

@stop
