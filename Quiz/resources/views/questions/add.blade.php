@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-md-6 col-md-offset-3">  
    <h1>Add a question to <br> {{$quiz -> title}}</h1>
  
    <form method="POST" action="store" class="col-md-12">
      {{ csrf_field() }}
      <div class="form-group @if ($errors->has('question')) has-error @endif">
       <label for="questionInput"> question </label>
       <textarea name="question" class="form-control" placeholder="question" rows="1">{{old('question')}}</textarea>
      </div>
      <!--
      @for ($i = 0; $i < 3; $i++)
        <div class="form-group @if ($errors->has('answer{{$i+1}}')) has-error @endif">
          <label for="answerInput{{$i+1}}"> answer{{$i+1}}</label>
          <textarea name="answer{{$i+1}}" class="form-control" placeholder="answer{{$i+1}}" rows="1"></textarea>
        </div>
      @endfor
      !-->
      <div class="form-group @if ($errors->has('answer1')) has-error @endif">
       <label for="answerInput1"> answer1 </label>
       <textarea name="answer1" class="form-control" placeholder="answer1" rows="1">{{old('answer1')}}</textarea>
      </div>
      
      <div class="form-group @if ($errors->has('answer2')) has-error @endif">
       <label for="answerInput2"> answer2 </label>
       <textarea name="answer2" class="form-control" placeholder="answer2" rows="1">{{old('answer2')}}</textarea>
      </div>
      
      <div class="form-group @if ($errors->has('answer3')) has-error @endif">
       <label for="answerInput3"> answer3 </label>
       <textarea name="answer3" class="form-control" placeholder="answer3" rows="1">{{old('answer3')}}</textarea>
      </div>
      
      <div class="form-group">
        <label for="correctAnswer"> correct answer </label>
        <select name="correctAnswer" class="form-control">
          @for ($i = 0; $i < 3; $i++)
            <option>{{$i+1}}</option>
          @endfor
        </select>
      </div>
          
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group @if ($errors->has('ordinal')) has-error @endif">
           <label for="ordinal"> ordinal </label>
           <input name="ordinal" type="number" min="0" class="form-control" placeholder="ordinal">{{old('ordinal')}}</input>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-4 form-group @if ($errors->has('youtubeId')) has-error @endif">
          <label for="youtubeId"> youtube id </label>
          <textarea type="text" name="youtubeId" class="form-control" placeholder="youtube Id" rows="1">{{old('youtubeId')}}</textarea>
        </div>
        <div class="col-xs-4 form-group @if ($errors->has('youtubeStart')) has-error @endif">
          <label for="youtube_start"> youtube start </label>
          <input type="number" value="0" min="0" name="youtubeStart" class="form-control" placeholder="youtube start"></input>
        </div>
        <div class="col-xs-4">
          <label for="youtube_end"> youtube end </label>
          <input type="number" min="0" name="youtubeEnd" class="form-control" placeholder="youtube end" value="{{old('youtubeEnd')}}"></input>
        </div>
      </div>

      <div class="form-group" style="margin-top:30px">
        <button type="submit" class="btn btn-default"> Add question </button>
      </div>
    </form>  
      
  </div>
</div>
@stop
