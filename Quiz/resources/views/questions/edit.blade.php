@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-md-6 col-md-offset-3">  
    <h1>Edit a question<br> </h1>
    
    <form method="POST" action="/question/{{ $question->id }}" class="col-md-12" enctype="multipart/form-data">
      {{ csrf_field() }}
      {{ method_field('PATCH') }}
      <div class="form-group @if ($errors->has('question')) has-error @endif">
       <label for="questionInput"> question </label>
       <textarea name="question" class="form-control" placeholder="question" rows="1">{{ $question->question}}</textarea>
      </div>
      
      @for ($i = 0; $i < count($answers); $i++)
        <div class="form-group @if ($errors->has('$answer{{$i+1}}')) has-error @endif">
          <label for="answerInput{{$i+1}}"> answer{{$i+1}}</label>
          <textarea name="answer{{$i+1}}" class="form-control" placeholder="answer{{$i+1}}" rows="1">{{ $answers[$i]}}</textarea>
        </div>
      @endfor
        
      <div class="form-group">
        <label for="correctAnswer"> correct answer </label>
        <select name="correctAnswer" class="form-control">
          @for ($i = 0; $i < count($answers); $i++)
              <option @if($question->correct == strval($i+1)) selected @endif>{{$i+1}}</option>
          @endfor
        </select>
      </div>
      
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group @if ($errors->has('ordinal')) has-error @endif">
           <label for="ordinal"> ordinal </label>
           <input type="number" min="0" name="ordinal" class="form-control" placeholder="ordinal" value="{{$ordinal}}"></input>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-4 form-group @if ($errors->has('youtubeId')) has-error @endif">
          <label for="youtubeId"> youtube id </label>
          <textarea type="text" name="youtubeId" class="form-control" placeholder="youtube Id" rows="1">{{ $question->youtube_id}}</textarea>
        </div>
        <div class="col-xs-4 form-group @if ($errors->has('youtubeStart')) has-error @endif">
          <label for="youtube_start"> youtube start </label>
          <input type="number" min="0" name="youtubeStart" class="form-control" placeholder="youtube start" value="{{$youtubeStartNum}}"></input>
        </div>
        <div class="col-xs-4">
          <label for="youtube_end"> youtube end </label>
          <input type="number" min="0" name="youtubeEnd" class="form-control" placeholder="youtube end" value="{{$youtubeEndNum}}"></input>
        </div>
      </div>

      <div class="form-group" style="margin-top:30px">
        <button type="submit" class="btn btn-default"> Edit question </button>
      </div>
    </form>  
    @if(count($errors))
      <ul>
        @foreach($errors->all() as $error)
          <li> {{ $error }} </li>
        @endforeach
      </ul>
    @endif
  </div>
</div>
  
@stop

  


