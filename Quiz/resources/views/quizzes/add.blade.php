@extends('layouts.app')

@section('content')

    <div class="row">
      <div class="col-md-6 col-md-offset-3">
          
        <form method="POST" action="new" class="col-md-12">
          {{ csrf_field() }}
          <div class="form-group @if ($errors->has('quiz_title')) has-error @endif">
           <label for="quizInputTitle"> Quiz title </label>
           <input type="text" name="quiz_title" class="form-control" placeholder="title"></input>
          </div>
          
          <div class="form-group">
           <button type="submit" class="btn btn-default"> Add quiz </button>
          </div>
        </form>
        @if(count($errors))
          <ul>
            @foreach($errors->all() as $error)
              <li> {{ $error }} </li>
            @endforeach
          </ul>
        @endif
      </div>
    </div>
    


@stop