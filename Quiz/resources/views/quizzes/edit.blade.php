@extends('layouts.app')

@section('content')

    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        
        <h1>Edit quiz</h1>
        
        <form method="POST" class="col-md-12" action="/quizzes/{{ $quiz->id }}" enctype="multipart/form-data" style="padding-bottom:15px">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          
            <div class="row">
              <div class="col-xs-8 form-data @if ($errors->has('quiz_title')) has-error @endif">
                <label for="quizEditTitle"> Quiz title </label>
                <textarea name="quiz_title" class="form-control" placeholder="title" rows="1">{{ $quiz->title }}</textarea>
              </div>
          
              <div class="form-group">
               <button style="margin-top:25px; float:right;" type="submit" class="btn btn-default">Update title</button>
              </div>
            </div>
        </form>
        
        <div style="padding-left:16px">
          <label for="qustions">Questions</label>
        </div>
          
        <div class="row">   
          <div class="col-xs-12">
            <ul class='list-group' style="padding-left:15px;">
              @foreach ($quiz->questions as $question)
                <li class='list-group-item'>    
                    {{ $question->question }}
                  <div style="float:right">
                    <a style="display: inline-block;" href="/question/{{$question->id}}/edit">
                      edit
                    </a>
                    <p style="display: inline-block;">/</p>            
                    <div style="display: inline-block;">
                      <form method="POST" action="/quizzes/{{ $quiz->id }}/{{ $question->id }}/delete" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button style="padding:0px; padding-bottom:2px;" type="submit" class="btn btn-link">delete</button>
                      </form>
                    </div>    
                  </div>
                </li>
              @endforeach
            </ul>
          </div>
         </div>
        <a style="padding-top:8px; margin-left:15px;" href="/quizzes/{{$quiz->id}}/add-question" class="btn btn-default">Add a question</a>
      </div>
    </div>
    
@stop