@extends('layouts.app')

@section('content')

    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        
        <h1>{{ $quiz -> title }}</h1>
        
        <ul>
          @foreach ($quiz->questions as $question)
            <li>{{ $question->question }}</li>
          @endforeach
        </ul>
          
      </div>
    </div>
    
@stop