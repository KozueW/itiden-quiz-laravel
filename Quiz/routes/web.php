<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/', 'QuizzesController@home');

Auth::routes();

Route::get('/home', 'QuizController@index');
Route::get('/quizzes/add', 'QuizController@add');
Route::get('/quizzes/{quiz}', 'QuizController@show');

Route::post('quizzes/new', 'QuizController@store');

Route::get('quizzes/{quiz}/edit', 'QuizController@edit');
Route::patch('quizzes/{quiz}', 'QuizController@update');
Route::delete('quizzes/{quiz}/delete', 'QuizController@destroy');

Route::get('quizzes/{quiz}/add-question', 'QuestionController@add');
Route::post('quizzes/{quiz}/store', 'QuestionController@store');
Route::delete('quizzes/{quiz}/{question}/delete', 'QuestionController@destroy');

Route::get('/question/{question}/edit', 'QuestionController@edit');
Route::patch('question/{question}', 'QuestionController@update');

